// Soal 1

var nilai = 82;

if (nilai >= 85) {
  console.log('Predikat A');
} else if (nilai >= 75 && nilai < 85) {
  console.log('Predikat B');
} else if (nilai >= 65 && nilai < 75) {
  console.log('Predikat C');
} else if (nilai >= 55 && nilai < 55) {
  console.log('Predikat D');
} else {
  console.log('Predikat E');
}

// Soal 2
var tanggal = 6;
var bulan = 8;
var tahun = 1998;

switch (bulan) {
  case 1: {
    console.log('Januari');
    break;
  }
  case 2: {
    console.log('Februari');
    break;
  }
  case 3: {
    console.log('Maret');
    break;
  }
  case 4: {
    console.log('April');
    break;
  }
  case 5: {
    console.log('Mei');
    break;
  }
  case 6: {
    console.log('Juni');
    break;
  }
  case 7: {
    console.log('Juli');
    break;
  }
  case 8: {
    console.log('Agustus');
    break;
  }
  case 9: {
    console.log('September');
    break;
  }
  case 10: {
    console.log('Oktober');
    break;
  }
  case 11: {
    console.log('November');
    break;
  }
  case 12: {
    console.log('Desember');
    break;
  }
  default: {
    console.log('Tidak Ada Bulan 13 dan seterusnya');
  }
}

var hasil = tanggal + bulan + tahun;
console.log(hasil);

// Soal 3
var bintang = '';

for (var i = 0; i < 7; i++) {
  for (var j = 0; j <= i; j++) {
    bintang += '* ';
  }
  bintang += '\n';
}

console.log(bintang);

// Soal 4
var m = 5;

for (var i = 1; i <= m; i++) {
  console.log(i + ' - I Love ');
}
