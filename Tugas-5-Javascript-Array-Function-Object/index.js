// Soal 1

var daftarHewan = [
  '2. Komodo',
  '5. Buaya',
  '3. Cicak',
  '4. Ular',
  '1. Tokek',
];

daftarHewan.sort();

for (i = 0; i < daftarHewan.length; i++) {
  console.log(daftarHewan[i]);
}

// Soal 2

function introduce(name, age, address, hobby) {
  var name = name;
  var age = age;
  var address = address;
  var hobby = hobby;

  return (
    'Nama saya ' +
    data.name +
    ', umur saya ' +
    data.age +
    ' tahun, alamat saya di ' +
    data.address +
    ', dan saya punya hobby yaitu' +
    data.hobby
  );
}

var data = {
  name: 'John',
  age: 30,
  address: 'Jalan Pelesiran',
  hobby: 'Gaming',
};
var perkenalan = introduce(data);
console.log(perkenalan);

// Soal 3
var vokal = ['a', 'i', 'u', 'e', 'o'];
function hitung_huruf_vokal(str) {
  var jumlah = 0;
  for (var kata of str.toLowerCase()) {
    if (vokal.includes(kata)) {
      jumlah++;
    }
  }

  return jumlah;
}

var hitung_1 = hitung_huruf_vokal('Muhammad');

var hitung_2 = hitung_huruf_vokal('Iqbal');

console.log(hitung_1, hitung_2); // 3 2

// Soal 4
function hitung(num) {
  var angka = -2;
  if (num == 0) {
    return angka;
  } else {
    for (i = 0; i < num; i++) {
      return hitung(num - 1) - hitung(angka + 2);
    }
  }
}

console.log(hitung(0)); // -2
console.log(hitung(1)); // 0
console.log(hitung(2)); // 2
console.log(hitung(3)); // 4
console.log(hitung(5)); // 8
