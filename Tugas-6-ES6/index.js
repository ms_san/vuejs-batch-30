//Spal 1
let luas = (p, l) => p * l;
console.log(luas(5, 2));

let keliling = (p, l) => 2 * (p + l);
console.log(keliling(5, 2));

//Soal 2
const newFunction = (firstName, lastName) => {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`);
    },
  };
};

//Driver Code
newFunction('William', 'Imoh').fullName();

//Soal 3
const newObject = {
  firstName: 'Muhammad',
  lastName: 'Iqbal Mubarok',
  address: 'Jalan Ranamanyar',
  hobby: 'playing football',
};
const { firstName, lastName, address, hobby } = newObject;

//Driver Code
console.log(firstName, lastName, address, hobby);

//Soal 4
const west = ['Will', 'Chris', 'Sam', 'Holly'];
const east = ['Gill', 'Brian', 'Noel', 'Maggie'];

const combined = [...west, ...east];
//Driver Code
console.log(combined);

//Soal 5
const planet = 'earth';
const view = 'glass';
let before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} `;

console.log(before);
