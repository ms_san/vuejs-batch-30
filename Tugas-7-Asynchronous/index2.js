let readBooksPromise = require('./promise.js');

let books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
  { name: 'komik', timeSpent: 1000 },
];

const init = async () => {
  await readBooksPromise(10000, books[3]);
};

init();
